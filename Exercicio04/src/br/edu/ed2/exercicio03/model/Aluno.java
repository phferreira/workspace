package br.edu.ed2.exercicio03.model;

import br.edu.ed2.exercicio03.util.DateUtil;

public class Aluno {
	private Integer matricula;
	private String nome;
	private java.util.Date dataNascimento;
	private char sexo;
	private String curso;
	
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public java.util.Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(java.util.Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getSexoExtenco() {
		if (getSexo() == 'F') {
			return "Feminino";
		} else {
			return "Masculino";
		} 
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * Retorna todos os atributos da classe em String
	 */
	public String toString(){
		return (getMatricula() + " " + getNome() + " " + DateUtil.convertDateToString(getDataNascimento()) + " " + getSexo() + " " + getCurso()); 
	}
	
	/*
	 * Retorna os atributos com separador '$'
	 */
	public String toStringArquivo(){
		return (getMatricula() + "$" + getNome() + "$" + DateUtil.convertDateToString(getDataNascimento()) + "$" + getSexo() + "$" + getCurso()); 
	}
	
	/*
	 * Retorna os atributos com matricula formatada com 15 caracteres
	 * Nome em maiusculo
	 * Sexo por exten�o  
	 */
	public String toStringArquivoFormat(){
		return (
				String.format("%015d", getMatricula()) + "$" + 
				getNome().toUpperCase() + "$" +
				getSexoExtenco() + "$" +
				DateUtil.convertDateToString(getDataNascimento()) + "$" +
				getCurso()	
				);
	}
	
}
