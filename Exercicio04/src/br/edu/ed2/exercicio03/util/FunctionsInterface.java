package br.edu.ed2.exercicio03.util;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import br.edu.ed2.exercicio03.model.Aluno;

public interface FunctionsInterface {

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getData()
	 */
	public abstract String getData();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getSexo()
	 */
	public abstract String getSexo();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getCurso()
	 */
	public abstract String getCurso();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getNomeMasculino()
	 */
	public abstract String getNomeMasculino();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getNomeFeminino()
	 */
	public abstract String getNomeFeminino();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getSegundoNome()
	 */
	public abstract String getSegundoNome();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getUltimoNome()
	 */
	public abstract String getUltimoNome();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getAlunoSequencial(int)
	 */
	public abstract Aluno getAlunoSequencial(int matricula) throws IOException,
			ParseException;

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getAlunoIndexado(int)
	 */
	public abstract Aluno getAlunoIndexado(int matricula) throws IOException,
			ParseException;

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraIndiceAlunos()
	 */
	public abstract void geraIndiceAlunos() throws IOException;

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraAlunos()
	 */
	public abstract void geraAlunos();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraIndiceInvertidoAlunos()
	 */
	public abstract void geraIndiceInvertidoAlunos() throws IOException;
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#startLucene(String codigo)
	 */
	public abstract ArrayList<Aluno> startLucene(String codigo);

}