package br.edu.ed2.arquivocsv.principal;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JScrollBar;

import java.awt.Window.Type;
import java.awt.Panel;
import java.awt.TextArea;

public class Principal extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 586, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Quantidade de linhas");
		lblNewLabel.setBounds(12, 332, 274, 15);
		contentPane.add(lblNewLabel);
		
		JLabel lblQuantidadeDeColunas = new JLabel("Quantidade de colunas:");
		lblQuantidadeDeColunas.setBounds(12, 359, 276, 15);
		contentPane.add(lblQuantidadeDeColunas);
		
		JLabel lblTempoDeExecuo = new JLabel("Tempo de execução:");
		lblTempoDeExecuo.setBounds(12, 386, 276, 15);
		contentPane.add(lblTempoDeExecuo);
		
		TextArea txtDados = new TextArea();
		txtDados.setBounds(10, 10, 566, 274);
		contentPane.add(txtDados);
		
		JButton btnNewButton = new JButton("Mostrar");
		btnNewButton.setBounds(417, 354, 117, 25);
		contentPane.add(btnNewButton);
		
		class threadArquivo implements Runnable {
			public void run(){
				try {
					Scanner arquivo = new Scanner(new File("resources/arquivos/arquivo.csv"));
					Double tempoInicio = (double) System.currentTimeMillis();
					Integer quantidadeLinha = 0;
					Integer quantidadeColuna = 0;
					Integer countLinha = 0;
					String linha = "";
					txtDados.setText("");
					lblNewLabel.setText("Quantidade de linhas: ");
					lblQuantidadeDeColunas.setText("Quantidade de colunas: ");					
					linha = arquivo.nextLine() + "\n";
					quantidadeColuna = linha.split(",").length;
					while (arquivo.hasNextLine()) {	
						linha += arquivo.nextLine() + "\n";
						quantidadeLinha ++;
						if (quantidadeLinha % 500 == 0) {
							txtDados.insert(linha, txtDados.getCaretPosition());
							countLinha += linha.length();
							txtDados.setCaretPosition(countLinha);
							linha = "";
						}
						lblTempoDeExecuo.setText("Tempo de execução: " + ((System.currentTimeMillis() - tempoInicio) / 1000));
					}					
					arquivo.close();
					lblNewLabel.setText("Quantidade de linhas: " + quantidadeLinha);
					lblQuantidadeDeColunas.setText("Quantidade de colunas: " + quantidadeColuna);
					btnNewButton.setEnabled(true);
				} catch (FileNotFoundException e) {
					e.printStackTrace();				
				}
			}
		}
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewButton.setEnabled(false);
				threadArquivo threadarquivo = new threadArquivo();
				Thread mostraArquivo = new Thread(threadarquivo);
				mostraArquivo.start(); 
				
			}
		});
					
	}
}
