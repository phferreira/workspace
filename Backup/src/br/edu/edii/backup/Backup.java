package br.edu.edii.backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Backup {

	public static void main(String[] args) throws IOException {
		long tempoInicio = System.currentTimeMillis();
		InputStream is = new FileInputStream("foto.jpg");
		File fotoBackup = new File("foto_backup.jpg");
		FileOutputStream os = new FileOutputStream(fotoBackup);
		int bytes = -1;
		int contBytes = 0;
		
		bytes = is.read();
		while (bytes != -1){
			os.write(bytes);
			bytes = is.read();
			contBytes++;
		}
		is.close();
		os.close();
		
		System.out.println("Tempo de execu��o: " + (System.currentTimeMillis() - tempoInicio) + "ms");
		System.out.println("Quantidade de bytes: " + contBytes + " bytes.");
		System.out.println(fotoBackup.getAbsolutePath());
	}
}
