package br.edu.ed2.questao1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Questao01 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		URL endereco = new URL("http://www.google.com");
		BufferedReader leitura = new BufferedReader(new InputStreamReader(endereco.openStream()));
		
		Boolean bImg = false, bTitle = false, bInput = false;
		Integer contador = 1, maior = -99999, menor = 99999;
		Integer linhaMenor = 0, linhaMaior = 0, quantidadeBytes = 0;
		Integer cont = 0, quantidadeChar = 0;
		String linha;
		
		while ((linha = leitura.readLine()) != (null)){
			if (linha.contains("img")){
				bImg = true;
			}
			if (linha.contains("title")){
				bTitle = true;
			}
			if (linha.contains("input")){
				bInput = true;
			}
			for (int i = 0; i < linha.length(); i++) {
				if (linha.charAt(i) != ' ')
					cont ++;				
			}
			if (cont > maior){
				maior = cont;
				linhaMaior = contador;
			}
			if (cont < menor && cont > 0){
				menor = cont;
				linhaMenor = contador;
			}
			quantidadeBytes += linha.getBytes().length;
			quantidadeChar  += linha.length();
			contador ++;
			System.out.println(linha.toString());
			cont = 0;
		}
		System.out.printf("Quantidade de caracteres da pagina: %d \n",quantidadeChar);
		System.out.printf("Quantidade de bytes da pagina: %d \n", quantidadeBytes);
		System.out.printf("Linha maior com %d caracteres na linha %d \n", maior, linhaMaior);
		System.out.printf("Linha menor com %d caracteres na linha %d \n", menor, linhaMenor);
		System.out.print("Contem todas as tags: ");
		if (bImg && bInput && bTitle) 
			System.out.print("SIM");
		else
			System.out.print("NAO");
	}

}
