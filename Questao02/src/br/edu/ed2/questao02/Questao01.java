package br.edu.ed2.questao02;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Questao01 {

	public static void main(String[] args) throws IOException {
		long time = System.currentTimeMillis();
		InputStream imagemLer = new FileInputStream("imagem.png");
		File imagemCopia = new File("imagem(Cópia).png");
		FileOutputStream imagemEscrever = new FileOutputStream(imagemCopia);
		int bytes = -1;
		int Bytes = 0;
		
		bytes = imagemLer.read();
		while (bytes != -1){
			imagemEscrever.write(bytes);
			bytes = imagemLer.read();
			Bytes++;
		}
		imagemLer.close();
		imagemEscrever.close();
		
		System.out.println("Tempo de execução: " + (System.currentTimeMillis() - time) + "ms");
		System.out.println("Quantidade de bytes: " + Bytes + " bytes.");
		System.out.println(imagemCopia.getAbsolutePath());

	}

}
