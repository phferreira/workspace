package br.edu.ed2.questao13.userinterface;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.edu.ed2.questao13.model.Tcc;
import br.edu.ed2.questao13.util.DateUtil;
import br.edu.ed2.questao13.util.Functions;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Questao13 extends JFrame {

	private static final String PATH = "resoucers/index";
	private JPanel contentPane;
	private static JTextField txtCodigo;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	String[] colunas = new String[] {"Código", "Aluno", "Orientador", "Título", "Resumo"};
	public String[][] linha = new String[1000][colunas.length];
	public String nomeBusca;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Questao13 frame = new Questao13();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Questao13() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 546, 370);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel pnlTabela = new JPanel();
		pnlTabela.setBounds(10, 199, 510, 131);
		contentPane.add(pnlTabela);

		txtCodigo = new JTextField();
		txtCodigo.setBounds(71, 11, 213, 19);
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);

		JLabel lblCdigo = new JLabel("Pesquisa:");
		lblCdigo.setBounds(12, 14, 57, 15);
		contentPane.add(lblCdigo);

		Panel panel = new Panel();
		panel.setBounds(10, 62, 274, 65);
		contentPane.add(panel);
		panel.setLayout(null);

		JRadioButton rdbtnIndexada = new JRadioButton("Código");
		buttonGroup.add(rdbtnIndexada);
		rdbtnIndexada.setSelected(true);
		rdbtnIndexada.setBounds(20, 35, 90, 23);
		panel.add(rdbtnIndexada);

		JRadioButton rdbtnInvertida = new JRadioButton("Titulo ou Resumo");
		buttonGroup.add(rdbtnInvertida);
		rdbtnInvertida.setBounds(112, 35, 109, 23);
		panel.add(rdbtnInvertida);

		JLabel lblTempoIndexado = new JLabel("Localizar por:");
		lblTempoIndexado.setBounds(10, 9, 274, 19);
		panel.add(lblTempoIndexado);

		JTable tabela = new JTable();
		tabela.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					// duplo clique
				}
			}
		});
		pnlTabela.setLayout(new GridLayout(1, 1));
		JScrollPane barraRolagem = new JScrollPane(tabela);
		pnlTabela.add(barraRolagem);
		getContentPane().add(pnlTabela);
		DefaultTableModel modelo = new DefaultTableModel(colunas, 0);
		tabela.setModel(modelo);

		JButton btnProcurar = new JButton("Pesquisar");
		btnProcurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Functions funcao = new Functions();
				Tcc tcc = new Tcc();
				ArrayList<Tcc> tccLista = new ArrayList<Tcc>();

				if (rdbtnIndexada.isSelected()) {
					nomeBusca = "Código ";
					linha = funcao.limparLista(linha, 1);
					try {
						tcc = funcao.getAlunoIndexado(Integer
								.parseInt(txtCodigo.getText()));
					} catch (IOException | NumberFormatException
							| ParseException e) {
						e.printStackTrace();
					}
				} else {
					tccLista = funcao.startLucene(txtCodigo.getText());
					linha = funcao.limparLista(linha, tccLista.size());
					nomeBusca = "Titulo ou resumo ";
				}

				if (tccLista.size() != 0) {
					int i = 0;
					for (Tcc tcc2 : tccLista) {
						linha[i][0] = tcc2.getMatricula().toString();
						linha[i][1] = tcc2.getAluno();
						linha[i][2] = tcc2.getOrientador();
						linha[i][3] = tcc2.getTitulo();
						linha[i][4] = tcc2.getResumo();
						i++;
					}
				} else if (tcc.getAluno() != null) {
					linha[0][0] = tcc.getMatricula().toString();
					linha[0][1] = tcc.getAluno();
					linha[0][2] = tcc.getOrientador();
					linha[0][3] = tcc.getTitulo();
					linha[0][4] = tcc.getResumo();
				} else {
					JOptionPane.showMessageDialog(null,
							nomeBusca + txtCodigo.getText()
									+ " não encontrado.");
				}

				DefaultTableModel modelo = new DefaultTableModel();
				modelo.setDataVector(linha, colunas);
				tabela.setModel(modelo);
				tabela.revalidate();
			}
		});
		btnProcurar.setBounds(294, 9, 117, 25);
		contentPane.add(btnProcurar);
	}
}
