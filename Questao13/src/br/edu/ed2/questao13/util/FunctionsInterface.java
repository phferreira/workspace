package br.edu.ed2.questao13.util;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import br.edu.ed2.questao13.model.Tcc;

public interface FunctionsInterface {

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getData()
	 */
	public abstract String getData();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getSexo()
	 */
	public abstract String getSexo();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getCurso()
	 */
	public abstract String getCurso();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getNomeMasculino()
	 */
	public abstract String getNomeMasculino();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getNomeFeminino()
	 */
	public abstract String getNomeFeminino();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getSegundoNome()
	 */
	public abstract String getSegundoNome();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getUltimoNome()
	 */
	public abstract String getUltimoNome();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getAlunoSequencial(int)
	 */
	public abstract Tcc getAlunoSequencial(int matricula) throws IOException,
			ParseException;

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getAlunoIndexado(int)
	 */
	public abstract Tcc getAlunoIndexado(int matricula) throws IOException,
			ParseException;

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraIndiceAlunos()
	 */
	public abstract void geraIndiceTcc() throws IOException;

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraAlunos()
	 */
	public abstract void geraAlunos();

	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraIndiceInvertidoAlunos()
	 */
	public abstract void geraIndiceInvertidoAlunos() throws IOException;
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#startLucene(String codigo)
	 */
	public abstract ArrayList<Tcc> startLucene(String codigo);

	public abstract String getProfessor();

	public abstract String getResumo();

	public abstract String getTitulo();

	public abstract Tcc getTccIndexado(int matricula) throws IOException, ParseException;

}