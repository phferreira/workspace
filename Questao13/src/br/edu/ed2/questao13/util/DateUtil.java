package br.edu.ed2.questao13.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author 267630
 *
 */
public class DateUtil {
	public final static String DATE_FORMAT = "dd/MM/yyyy";
	public final static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
	
	public static Date convertStringToDate(String data) throws ParseException{
		return sdf.parse(data);
	}
	
	public static String convertDateToString(Date data){
		return sdf.format(data);
	}
}
