package br.edu.ed2.questao13.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import br.edu.ed2.questao13.model.Tcc;

public class Functions implements FunctionsInterface  {
private static final String PATH = "resoucers/index";
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getData()
	 */
	@Override
	public String getData(){
		int dia = (int) (Math.random() * 30);
		int mes = (int) (Math.random() * 12);
		int ano = (int) (Math.random() * 50);
		if (dia == 0) {
			dia = 1;
		}
		if (mes == 0) {
			mes = 12;
		}
		return (dia + "/" + mes + "/" + (1997 - ano));
	}
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getSexo()
	 */
	@Override
	public String getSexo(){
		int numero = (int) (Math.random() * 2); 
		switch (numero) {
		case 0:
			return "M";
		case 1:
			return "F";
		default:
			return "";
		}
	}
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getCurso()
	 */
	@Override
	public String getProfessor(){
		int numero = (int) (Math.random() * 10); 
		switch (numero) {
		case 1:
			return "Roberson";
		case 2:
			return  "Otilia";
		case 3:
			return  "Albino s2s2";
		case 4:
			return  "Erno Shwatsnegger";
		case 5:
			return  "Marcelo String";
		case 6:
			return  "Naiara Febem";
		case 7:
			return  "Paulo Bradesco";
		case 8:
			return  "Evelaço";
		case 9:
			return  "Marcia Plante";
		default:
			return  "Alisson";
		}
	}
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getNomeMasculino()
	 */
	@Override
	public String getNomeMasculino(){
		int numero = (int) (Math.random() * 10); 
		switch (numero) {
		case 1:
			return "Pedro ";
		case 2:
			return  "Joao ";
		case 3:
			return  "Mateus ";
		case 4:
			return  "Carlos ";
		case 5:
			return  "Cesar ";
		case 6:
			return  "Marcos ";
		case 7:
			return  "Paulo ";
		case 8:
			return  "Willian ";
		case 9:
			return  "Antonio ";
		default:
			return  "Thiago ";
		}
	}	
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getNomeFeminino()
	 */
	@Override
	public String getNomeFeminino(){
		int numero = (int) (Math.random() * 10); 
		switch (numero) {
		case 1:
			return "Ana ";
		case 2:
			return  "Maria ";
		case 3:
			return  "Carla ";
		case 4:
			return  "Jaine ";
		case 5:
			return  "Isabela ";
		case 6:
			return  "Maluane ";
		case 7:
			return  "Deiziane ";
		case 8:
			return  "Luana ";
		case 9:
			return  "Luiza ";
		default:
			return  "Paula ";
		}
	}	
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getSegundoNome()
	 */
	@Override
	public String getSegundoNome(){
		int numero = (int) (Math.random() * 10); 
		switch (numero) {
		case 1:
			return "Do Carmo ";
		case 2:
			return  "Antunes ";
		case 3:
			return  "Ferreira ";
		case 4:
			return  "Dos Santos ";
		case 5:
			return  "Silva ";
		case 6:
			return  "Nunes ";
		case 7:
			return  "Alves ";
		case 8:
			return  "Da Cunha ";
		case 9:
			return  "Dos Santos ";
		default:
			return  " ";
		}
	}	
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getUltimoNome()
	 */
	@Override
	public String getUltimoNome(){
		int numero = (int) (Math.random() * 10); 
		switch (numero) {
		case 1:
			return "Jr.";
		case 2:
			return  "Neto";
		case 3:
			return  "Pinto";
		case 4:
			return  "Buganti";
		case 5:
			return  "Mezalira";
		case 6:
			return  "Souto";
		case 7:
			return  "Debiazi";
		case 8:
			return  "Cardoso";
		case 9:
			return  "Perin";
		default:
			return  "Novaes";
		}
	}	
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#getAlunoIndexado(int)
	 */
	@Override
	public Tcc getTccIndexado(int matricula) throws IOException, ParseException{
		Tcc tcc = new Tcc();
		RandomAccessFile dadosTcc = new RandomAccessFile("resoucers/dados/tcc_indice.txt", "r");		
		String linha = null;

		while((linha = dadosTcc.readLine()) != null) {
			String[] dados = linha.split("#");		
			if (Integer.parseInt(dados[1]) == matricula){
				matricula = Integer.parseInt(dados[0]);
				break;
			}
		}
		
		dadosTcc = new RandomAccessFile("resoucers/dados/tcc.txt", "r");
		dadosTcc.seek(matricula);
		String linha1 = dadosTcc.readLine();
		String[] dados = linha1.split("#");		
		tcc.setMatricula(Integer.parseInt(dados[0]));
		tcc.setAluno(dados[1]);
		tcc.setOrientador(dados[2]);
		tcc.setTitulo(dados[3]);
		tcc.setResumo(dados[4]);
		return tcc;
	}
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraIndiceAlunos()
	 */
	@Override
	public void geraIndiceTcc() throws IOException{
		Tcc tcc = new Tcc();
		RandomAccessFile dadosTcc = new RandomAccessFile("resoucers/dados/tcc.txt", "r");		
		String linha = null;
		PrintWriter escreveArquivo = new PrintWriter(new File("resoucers/dados/tcc_indice.txt"));
		while((linha = dadosTcc.readLine()) != null) {
			//System.out.println(linha);
			long indice = dadosTcc.getFilePointer() - (linha.length() + System.getProperty("line.separator").length());   //System.getProperty("line.separator").length() - "\r\n".length()
			//System.out.println(indice + " - " + linha);
			String[] a = linha.split("#");
			// gravando os dados do índice
			escreveArquivo.println(indice + "#" + a[0]);
			
		}
		escreveArquivo.flush();
		escreveArquivo.close();	
	}
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraAlunos()
	 */
	@Override
	public void geraAlunos(){
		//Criacao dos alunos
		PrintWriter pw;
		try {
			pw = new PrintWriter(new File("resoucers/dados/tcc.txt"));
			for (int i = 1; i <= 1000; i++) {
				String sexo = getSexo();
				String nome;
				if (sexo == "M") {nome = getNomeMasculino();} 
				else {nome = getNomeFeminino();} 
				pw.println(
						i + "#" + 
						nome + getSegundoNome() + getUltimoNome() + "#" +
						getProfessor() + "#" +
						getTitulo() + "#" +
						getResumo()							
				);
			}
			pw.flush();
			pw.close();	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#geraIndiceInvertidoAlunos()
	 */
	@Override
	public void geraIndiceInvertidoAlunos() throws IOException{
		Scanner arquivo = new Scanner(new File("resoucers/dados/tcc.txt"));
		Directory fsDirectory = FSDirectory.open(new File(PATH).toPath());
//		/*
//		 * Criar inst�ncia do analisador, que ser� usada para tokenizar os
//		 * dados de entrada
//		 */
		Analyzer standardAnalyzer = new StandardAnalyzer();

		// criacao inicial do index 
		IndexWriterConfig iwc = new IndexWriterConfig(standardAnalyzer);
		IndexWriter indexWriter = new IndexWriter(fsDirectory, iwc);
		//indexa��o
		//ler os dados do arquivo alunos txt e indexar o nome
		while (arquivo.hasNextLine()) {
			String linha = arquivo.nextLine();
			String dados[] = linha.split("#");
			
			// cria um novo campo para um documento
			Field campoMatricula = new Field("matricula", dados[0], Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field campoAluno = new Field("aluno",	dados[1], Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field campoOrientador = new Field("orientador", dados[2], Field.Store.YES, Field.Index.NOT_ANALYZED);
			Field campoTitulo = new Field("titulo",	dados[3], Field.Store.YES, Field.Index.ANALYZED);
		    Field campoResumo = new Field("resumo", dados[4], Field.Store.YES, Field.Index.ANALYZED);
			// cria um novo documento
			Document doc = new Document();
			doc.add(campoMatricula);
			doc.add(campoAluno);
			doc.add(campoOrientador);
			doc.add(campoTitulo);
			doc.add(campoResumo);

			// indexando o documento
			indexWriter.addDocument(doc);
		}
		indexWriter.close();
		fsDirectory.close();
	}
	/* (non-Javadoc)
	 * @see br.edu.ed2.exercicio03.util.FunctionsInterface#startLucene(String codigo)
	 */
	@Override
	public ArrayList<Tcc> startLucene(String codigo) {
		// Criar inst�ncia do Diret�rio
		// onde os arquivos de �ndice ser�o armazenados
		ArrayList<Tcc> tccLista = new ArrayList<Tcc>();
		try {			
			Scanner arquivo = new Scanner(new File("resoucers/dados/tcc.txt"));
			Directory fsDirectory = FSDirectory.open(new File(PATH).toPath());
//			/*
//			 * Criar instância do analisador, que será usada para tokenizar os
//			 * dados de entrada
//			 */
			Analyzer standardAnalyzer = new StandardAnalyzer();

			// Procurar nome com a palavra "aluno"
			// no campo de nome
			DirectoryReader ireader = DirectoryReader.open(fsDirectory); 
			IndexSearcher isearcher = new IndexSearcher(ireader);
			// Cria uma query de busca pelo nome com o termo: aluno
			// Retorna somente os 2 resultados mais significantes
			QueryParser parser = new QueryParser("titulo", standardAnalyzer);
			Query query = parser.parse(codigo);
			ScoreDoc[] hits = isearcher.search(query, 10).scoreDocs;
			// Lista os resultado encontrados, se houverem
			for (int i = 0; i < hits.length; i++) {
				Document hitDoc = isearcher.doc(hits[i].doc);
				Tcc tcc = new Tcc();
				tcc.setAluno(hitDoc.get("aluno"));
				tcc.setMatricula(Integer.parseInt((hitDoc.get("matricula"))));
				tcc.setOrientador(hitDoc.get("orientador"));
				tcc.setTitulo(hitDoc.get("titulo"));
				tcc.setResumo(hitDoc.get("sexo"));				
				tccLista.add(tcc);
//				System.out.println(hitDoc.get("nome") + hits[i].score);
				// aqui faz o seek
			}
			ireader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return tccLista;
	}
	
	public String[][] limparLista(String[][] lista, int linhas){
		return lista = new String[linhas][5];
	}
	
	@Override
	public String getTitulo(){
		int numero = (int) (Math.random() * 20); 
		switch (numero) {
		case 1:
			return "Criação de robôs.";

			case 2:
			return "Princípios básicos da computação.";

			case 3:
			return "A história da computação no Brasil.";

			case 4:
			return "Hardwares de proteção para empresas.";

			case 5:
			return "A produtividade do computador.";

			case 6:
			return "Computadores ligados em rede.";

			case 7:
			return "Ferramentas para extrair textos a partir de áudio ou vídeo.";

			case 8:
			return "Sistemas de controle de estoque para supermercados.";

			case 9:
			return "A tecnologia da informação para a medicina.";

			case 10:
			return "O que é o PLN e aonde pode ser usado.";

			case 11:
			return "A evolução do computador na segunda guerra mundial.";

			case 12:
			return "Como a segunda guerra ajudou na evolução do computador.";

			case 13:
			return "A importância do inglês como segundo idioma para o profissional.";

			case 14:
			return "A importância da internet para a globalização.";

			case 15:
			return "Analise de risco em sistemas de internet banking.";

			case 16:
			return "A importância do antivírus para as empresas.";

			case 17:
			return "A informática como agilidade no trabalho.";

			case 18:
			return "O comercio através da internet.";

			case 19:
			return "Como a internet sem fio funciona?";

			default:
			return "Ensino pela internet.";
		}
	}
	
	@Override
	public String getResumo(){
		int numero = (int) (Math.random() * 21); 
		switch (numero) {
			case 1: 
				return " Sistema criado para a gerencia do plantio, da criação de gado e vida agrária.";
	
			case 2: 
				return " Como criar um robô e como ele pode ser útil para a pessoa ou empresa.";
	
			case 3: 
				return " Buscar os princípios básicos para o profissional de computação exercer seu trabalho.";
	
			case 4: 
			    return " Como surgiu a computação no Brasil e como ela é aplicada até hoje.";
	
			case 5: 
			    return " Como criar hardwares para proteger os dados da empresa, e como atualizá-los.";
	
			case 6: 
			    return " Analisar qual a produtividade ideal do computador para cada pessoa, dependendo do seu uso e para o que será usado.";
	
			case 7: 
			    return " Como os computadores em rede podem ser útil em uma empresa.";
	
			case 8: 
			    return " Buscar as ferramentas idéias e para AC criação deste software";
	
			case 9: 
			    return " A importância do controle via sistema para que haja um controle maior e uma percepção de venda, lucro e necessidade de pedidos.";
	
			case 10: 
			    return " A facilidade de controlar cada exame, a listagem de medicamentos para determinada doença ou ate mesmo tratamentos.";
	
			case 11: 
			    return " É a técnica para processamento de linguagem natural pára a aplicação de textos livres da internet.";
	
			case 12: 
			    return " Como a segunda guerra ajudou na evolução do computador.";
	
			case 13: 
			    return " Como a segunda guerra ajudou na evolução do computador.";
	
			case 14: 
			    return " A maioria dos atalhos ou programas são em inglês, então este profissional deve ter o inglês como segunda língua.";
	
			case 15: 
			    return " A rapidez na informação, a interação de varias pessoas do mundo todo conectadas ao mesmo tempo.";
	
			case 16: 
			    return " Como os sistemas de internet banking podem ser falhos e as possíveis melhorias.";
	
			case 17: 
			    return " A importância do antivírus para que o computador esteja sempre funcionando e protegido contra possíveis danos e invasões.";
	
			case 18: 
			    return " A facilidade no trabalho, e a rapidez para pesquisa ou até mesmo para controle de caixa ou estoque e comunicação interna da empresa.";
	
			case 19: 
			    return " Como este mercado está crescendo e as garantias de entrega de produto, criação de sistemas de compra e venda pela internet.";
	
			case 20: 
			    return " A explicação técnica da rede wi-fi, como surgiu esta tecnologia e como funciona.";
	
			default: 
			    return " Como este método pode facilitar no ensino, e até onde é confiável.";
		}
	}

	@Override
	public String getCurso() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Tcc getAlunoSequencial(int matricula) throws IOException,
			ParseException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Tcc getAlunoIndexado(int matricula) throws IOException,
			ParseException {
		// TODO Auto-generated method stub
		return null;
	}
}
