package br.edu.ed2.questao13.model;

import br.edu.ed2.questao13.util.DateUtil;

public class Tcc {
	private Integer matricula;
	private String aluno;
	private String orientador;
	private String titulo;
	private String resumo;
	
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public String getAluno() {
		return aluno;
	}
	public void setAluno(String nome) {
		this.aluno = nome;
	}
	public String getOrientador() {
		return orientador;
	}
	public void setOrientador(String orientador) {
		this.orientador = orientador;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}	
}
