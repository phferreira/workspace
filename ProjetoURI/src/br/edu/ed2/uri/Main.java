package br.edu.ed2.uri;

import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		double N1 = teclado.nextDouble();	
		double N2 = teclado.nextDouble();
		double N3 = teclado.nextDouble();
		double N4 = teclado.nextDouble();
		double exame = 0;
		double media = ((N1 * 0.2) + (N2 * 0.3) + (N3 * 0.4) + (N4 * 0.1)) ;
		if (media >= 5 && media < 7) {
			exame = teclado.nextDouble();			
		}
		System.out.printf("Media: %.1f", media);
		System.out.println();
		if (media >= 7){
			System.out.print("Aluno aprovado.");
		}else if (media >= 5){
			System.out.print("Aluno em exame.");
			System.out.println();
			System.out.printf("Nota do exame: %.1f", exame);
			System.out.println();
			media = (media + exame) / 2;
			if (media >= 5){
				System.out.print("Aluno aprovado.");
			}else{
				System.out.print("Aluno reprovado.");
			}			
			System.out.println();
			System.out.printf("Media final: %.1f", media);
		}else{
			System.out.print("Aluno reprovado.");
		}
		System.out.println();
	}
}
