package br.edu.ed2.uri;

import java.io.IOException;
import java.util.Scanner;

public class Uri {

	public static void main(String[] args) throws IOException {
		float dinhero;
		int n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, troca;
		int m1 = 0, m2 = 0, m3 = 0, m4 = 0, m5 = 0, m6 = 0;
		Scanner teclado = new Scanner(System.in);
		dinhero = teclado.nextFloat();
		if (dinhero >= 0 && dinhero <= 1000000){
			do{
				if (dinhero >= 100){
					n1 ++;
					dinhero -= 100;
					troca = 1;
				} else if (dinhero >= 50){
					n2 ++;
					dinhero -= 50;
					troca = 1;
				}else if (dinhero >= 20){
					n3 ++;
					dinhero -= 20;
					troca = 1;
				}else if (dinhero >= 10){
					n4 ++;
					dinhero -= 10;
					troca = 1;
				}else if (dinhero >= 5){
					n5 ++;
					dinhero -= 5;
					troca = 1;
				}else if (dinhero >= 2){
					n6 ++;
					dinhero -= 2;
					troca = 1;
				}else if (dinhero >= 1){
					m1 ++;
					dinhero -= 1;
					troca = 1;
				}else if (dinhero >= 0.5){
					m2 ++;
					dinhero -= 0.5;
					troca = 1;
				}else if (dinhero >= 0.25){
					m3 ++;
					dinhero -= 0.25;
					troca = 1;
				}else if (dinhero >= 0.1){
					m4 ++;
					dinhero -= 0.1;
					troca = 1;
				}else if (dinhero >= 0.05){
					m5 ++;
					dinhero -= 0.05;
					troca = 1;
				}else if (dinhero >= 0.01){
					m6 ++;
					dinhero -= 0.01;
					troca = 1;
				}else{
					troca = 0;
				}
			}while(troca == 1);
			System.out.printf("NOTAS:");
			System.out.println();
			System.out.printf("%d nota(s) de R$ 100.00", n1);
			System.out.println();
			System.out.printf("%d nota(s) de R$ 50.00", n2);
			System.out.println();
			System.out.printf("%d nota(s) de R$ 20.00", n3);
			System.out.println();
			System.out.printf("%d nota(s) de R$ 10.00", n4);
			System.out.println();
			System.out.printf("%d nota(s) de R$ 5.00", n5);
			System.out.println();
			System.out.printf("%d nota(s) de R$ 2.00", n6);
			System.out.println();
			System.out.printf("MOEDAS:");
			System.out.println();
			System.out.printf("%d moeda(s) de R$ 1.00", m1);
			System.out.println();
			System.out.printf("%d moeda(s) de R$ 0.50", m2);
			System.out.println();
			System.out.printf("%d moeda(s) de R$ 0.25", m3);
			System.out.println();
			System.out.printf("%d moeda(s) de R$ 0.10", m4);
			System.out.println();
			System.out.printf("%d moeda(s) de R$ 0.05", m5);
			System.out.println();
			System.out.printf("%d moeda(s) de R$ 0.01", m6);
			System.out.println();
		}
	}

}

