package br.edu.ed2.junversity.userinterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.Format;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;

import br.edu.ed2.junversity.model.Aluno;
import br.edu.ed2.junversity.util.DateUtil;

public class JUniversity {

	public static void main(String[] args) {
		try {
			ArrayList<Aluno> alunos = new ArrayList<Aluno>();
			Scanner arquivo = new Scanner(new File("resoucers/dados/alunos.txt"));
			while (arquivo.hasNextLine()) {
				String[] campos = arquivo.nextLine().split("#");
				Aluno aluno = new Aluno();
				aluno.setMatricula((Integer.parseInt(campos[0])));
				aluno.setNome(campos[1].toUpperCase());
				aluno.setDataNascimento(DateUtil.convertStringToDate(campos[2]));
				aluno.setSexo(campos[3].charAt(0));
				aluno.setCurso(campos[4]);
				alunos.add(aluno);
			}
			PrintWriter pw = new PrintWriter(new File("resoucers/dados/alunos_bkp1.txt"));
			for (Aluno aluno : alunos) {
				pw.println(aluno.toString());
				System.out.println("hau");
			}
			pw.flush();
			pw.close();	
			arquivo.close();
		} catch (FileNotFoundException | ParseException e) {
			e.printStackTrace();
		}

	}

}
